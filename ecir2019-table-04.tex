\section{Utilizing Table2Vec Embeddings}
\label{sec:app}

In this section, we extend previous table population and retrieval methods by incorporating the Table2Vec embeddings that were introduced in Sect.~\ref{sec:nwe}. 
For all tasks, we keep our focus on relational tables.  It is assumed that entities mentioned in the table are recognized and disambiguated by linking them to entries in a knowledge base~\citep{Bhagavatula:2015:TEL}.
The table population task is considered in two flavors: row population and column population.  We shall refer to the input table $T$ as \emph{seed table}, in which the set of entities from the core column are referred as seed entities $E$, and the set of column labels are denoted as seed labels $L$.

%\vspace*{-0.5\baselineskip}
\subsection{Row population}

Row population is a task of returning a list of entities, based on their likelihood of being added to the core column of the seed table $T$ in the next row.  The ranking is established based on the similarity of a candidate entity $e$ to the seed table entities $E$.
% that are ranked in a descending order with the relevance with $E$, and the top ranking one is most likely to be added into the \emph{}  in the next row. 
In this task, we measure entity similarity by two approaches: using a knowledge base and using Table2Vec embeddings. 

%\vspace*{-0.5\baselineskip}
\subsubsection{Baselines}
\label{sub:sub:bkb}
We employ three probabilistic ranking methods from~\cite{Zhang:2017:ESA} as our baselines, which rank candidate entities according to $P(e|E)$.  Candidate entity selection is done as in~\cite{Zhang:2017:ESA}.
%
\begin{description}
    \item[BL1] Entity similarity is measured based on the similarity of relations of $e$, obtained from RDF triples, and those of the seed tables entities $E$.    
%    A relation of an entity $e$ is given by the remainder of a rdf triple with the $e$ excluded.
    \item[BL2] It uses the Wikipedia Link-based Measure~\citep{Milne:2008:ELM} to estimate the semantic relatedness of entities based on their outgoing links (in the knowledge base).
    \item[BL3] It relies on the Jaccard similarity between outgoing links of entities.
\end{description}

%\vspace*{-0.5\baselineskip}
\subsubsection{Using Table2Vec embeddings}
\label{sub:sub:mte}

Recall that we have two entity embeddings, Table2VecE and Table2VecE*. The former is trained on all entities contained in the table, while the latter considers only entities in the core column.  Given that the row population task focuses on the core column, we employ the Table2VecE* embeddings here.
%Table2VecE trains entities from all table cells, but for this task, entities not from the core column are most likely \sz{intruducing} noise for our training corpus. 
%Thus, Table2VecE* are employed for this task.
We measure the similarity of each candidate entity $e$, against the seed entities $e' \in E$, using the cosine similarity of their respective embedding vectors:
%Note that our candidates are selected by returning top-$k$ ranked entities based on relevance between $e'$ and $e$.
%
\begin{equation}\label{eq:4}
    sim(e, E)= \frac{1}{|E|}\sum_{e' \in E}sim(e, e') = \frac{1}{|E|}\sum_{e' \in E}\frac{\vec {v_{e}} \boldsymbol{\cdot} \vec{v_{e'}}}{\lVert{\vec {v_{e}} \rVert \lVert{\vec {v_{e'}} \rVert}}} ~,
\end{equation}
%
where $|E|$ is the size of seed entity set, and $\vec {v_{e}}$ and $\vec {v_{e'}}$ are the embedding vectors of the candidate and seed entities, respectively. 

We then combine the baseline similarity with the Table2Vec-based similarity using the following linear mixture: 
%
\begin{equation}
    P(e|E) = \alpha \, P_{kb}(e|E) + (1 - \alpha) \, P_{emb}(e|E) ~,
    \label{eq:rp:comb}
\end{equation}
%
where $P_{\mathit{kb}}$ is the similarity measured using the knowledge base and $P_{\mathit{emb}}$ is based on table embeddings, and equals to Eq.~\eqref{eq:4}. 
%\ld{Note that here we employ the candidates in~\ref{sub:sub:bkb}}.

%\vspace*{-0.5\baselineskip}
%
\subsection{Column population}

Column population is the task of returning a ranked list of labels, $l_{1},\ldots,l_{k}$, given a seed table $T$.  The returned labels are ranked based on their relevance to the seed labels $L$. Similarly to row population, we consider two label similarity measures.

%\vspace*{-0.5\baselineskip}
\subsubsection{Baseline}
The baseline method, using a table corpus, is taken from~\cite{Zhang:2017:ESA}.  First, relevant tables are retrieved from the table corpus. Then, the probability of a candidate label being relevant $P(l|L)$ is estimated based on the occurrences of that label in relevant tables.  %Table relevance is established using 

%\vspace*{-0.5\baselineskip}
\subsubsection{Using Table2Vec embeddings}

We utilize embeddings trained on table headings, Table2VecH, for column label relevance estimation.  Similarly to row population, we employ cosine similarity based on the embedding vectors of the candidate label $l$ and seed labels $l' \in L$. 
Then, we combine these baseline estimate with the embedding-based similarity using: 
%
\begin{equation}
    P(l|L) = \alpha \, P_{\mathit{kb}}(l|L) + (1 - \alpha) \, P_{\mathit{emb}}(l|L) ~.
    \label{eq:cp:comb}
\end{equation}
%
%where the computing of $P_{\mathit{emb}}(l|L)$ follows analogously to Eq.~\ref{eq:4}.}

%\vspace*{-0.5\baselineskip}
\subsection{Table Retrieval}

%
\input{other-inputs/tab-features.tex}

Table retrieval is the task of returning a ranked list of tables in response to a keyword query $q$, based on their relevance to $q$. 
For this task, we employ a feature-based method as a baseline, which is referred to as the LTR method in~\citep{Zhang:2018:AHT}. LTR employs all the features listed in Table~\ref{tab:lsr}.  This method considers a rich set of query, table, and query-table features.  We utilize the word-based and entity-based table embeddings, Table2VecW and Table2VecE, to compute additional semantic matching features.  Specifically, each type of embedding contributes four features, for each of the similarity methods in~\citep{Zhang:2018:AHT}. 

\input{other-inputs/fig-fusion.tex}
%

Given that both the table and query are vectors now, we compute cosine similarity to measure relevance. For comparison purposes, we employ both methods in~\cite{Zhang:2018:AHT}: \emph{early fusion} and \emph{late fusion}. For the former method, query-table relevance is measured between centroid of query term vectors and centroid of table term vectors. The latter method conducts pairwise cosine similarity between table terms ($\vec t_{j}$) and query terms ($\vec q_{i}$) first, and then aggregates those results. The query-table relevance is then measured by an aggregation function, which consists of three aggregators: (i) maximum of $cosine(\vec q_{i}, \vec t_{j})$, (ii) sum of $cosine(\vec q_{i}, \vec t_{j})$ (iii) average of $cosine(\vec q_{i}, \vec t_{j})$. In this paper, we combine all four measures to yield the final similarity score; see Fig~\ref{fig:fusion} for an illustration. For performance comparison, we employ pre-trained Graph2Vec~\cite{Ristoski:2016:RGE} and Word2Vec embeddings~\citep{Mikolov:2013:DRW}.




