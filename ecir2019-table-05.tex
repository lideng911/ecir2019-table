\section{Evaluation}
\label{sec:eva}

In this section, we summarize our research questions (Sect.~\ref{sec:eva:req}), discuss our
experimental setup (Sect.~\ref{sec:eva:exs}), and then present our results and analysis for three table-related tasks (Sect.~\ref{sec:eva:row},~\ref{sec:eva:col},~\ref{sec:eva:ret})

\subsection{Research Questions}
\label{sec:eva:req}
The following research questions are addressed with our experiments: 
%
\begin{description}
    \item[RQ1] Can Table2Vec improve table population performance against the state-of-the-art baselines?
    \item[RQ2] Would word embeddings trained specifically on tables, as opposed to news, affect retrieval performance?
    \item[RQ3] Which of the semantic representations (entity vs. word embeddings) performs better in table retrieval?
\end{description}

%
\input{other-inputs/tab-corpus.tex}
%
%
\input{other-inputs/fig-eva-gt.tex}
%

%
%\vspace*{-0.5\baselineskip}
\subsection{Experimental Setup}
\label{sec:eva:exs}
For table population, we use Mean Average Precision (MAP) as the main metric and Mean Reciprocal Rank (MRR) as a supplementary metric for performance evaluation.  Table retrieval performance is evaluated by Normalized Discounted Cumulative Gain (NDCG) with a cut-off at 10 and 20. To test significance, we use a two-tailed paired t-test and write $\circ$ to denote not significant, and $\dag$/$\ddag$ to denote significance at the 0.05 and 0.01 levels, respectively.

We use the Wikipedia Tables corpus~\citep{Zhang:2017:ESA}, which contains 1.6 million high-quality relational tables, both for training the Table2Vec embeddings and for the retrieval experiments. %We detail the raw representations for training in Sect.~\ref{sec:nwe}. Additionally, 
For the word-based embedding, Table2VecW, we filter out empty strings, numbers, HTML tags, and stopwords from the raw text during training to obtain a better representation. For Table2VecH, we employ no normalization for the labels, i.e., ``year(s),'' ``year:,'' and ``year'' will be treated as different labels in our experiment. Table~\ref{tbl:emb:statistic} shows the statistics of different Table2Vec embeddings. DBpedia is used as our knowledge base, which is consistent with the original experiments in~\citep{Zhang:2018:AHT,Zhang:2017:ESA}. The test inputs and ground truth assessments are obtained for the three tasks as follows:

%
\input{other-inputs/fig-row-alpha.tex}
%


\begin{itemize}
	\item \emph{Row population:} we use the test set from~\citep{Zhang:2017:ESA}. It contains 1000 relational tables ($N$ rows and $M$ columns), of which each table has at least six rows and four columns. For evaluation, we take entities from the first $i$ rows ($i \in [1..5]$) as seed entities, and the remaining $(N-i)$ entities as ground truth. The test set contains $21,502$ unique entities, see Fig~\ref{fig:gt:row}.
	\item \emph{Column population:} we use the test set from~\citep{Zhang:2017:ESA}, consisting of 1000 relational tables. We take column heading labels from the first $j$ columns ($j \in [1..3]$) as seed labels, and the rest labels as ground truth. There are a total of $7,216$ unique column heading labels in this test set. Figure~\ref{fig:gt:col} illustrates the methodology of column population. 
	\item \emph{Table retrieval:} we use a set of 60 queries (two query subsets, QuerySet 1 and QuerySet 2) and corresponding ground truth relevance labels from~\cite{Zhang:2018:AHT}, which amount to a total of $3,120$ query-table pairs. QuerySet 1 consists of 30 queries collected via Amazon Mechanical Turk service. Through the service, web users were asked to suggest topics for a useful data table and supply URLs that provide an example table for the topic. QuerySet 2 is collected through analyzing query logs from Google Squared. All of its queries are a combination of a specific instance type and a property, e.g., ``food type,'' ``apples market share,'' and ``diseases risks.''
\end{itemize}

%
\input{other-inputs/tab-row.tex}
%
%
\input{other-inputs/fig-col-alpha.tex}
\input{other-inputs/tab-col.tex}
%

\subsection{Row population}
\label{sec:eva:row}
%\vspace*{-0.25\baselineskip}
%\subsubsection{Result}
The row population results are listed in Table~\ref{tbl:rows:rank}. The top three lines show the results of the baselines from the literature.  The bottom three lines are the results of combining the baselines with Table2VecE*.  
Note that the combination involves a mixture parameter $\alpha$ (cf. Eq.~\eqref{eq:rp:comb}).  To understand the potential of using table embeddings, we perform a grid search in steps of 0.1 for the value of $\alpha$, and report results using the $\alpha$ value that yielded the best MAP score, see Fig.~\ref{fig:row:alpha}.  The best performing $\alpha$ values for BL1, BL2, and BL3 are 0.4, 0.0, and 0.1, respectively. This means that the second baseline does not contribute at all to the combination.  %That is, the bottom two rows of Table~\ref{tbl:rows:rank} are based only on Table2VecE*, hence the scores in these two rows are identical.

Overall, we find that the combined methods outperform the respective baselines substantially and significantly ($p < 0.01 $). BL1 + Table2VecE* yields the best performance in terms of MAP.  It is worth pointing out that the performance of this combined methods improves more with more seed entities than the baseline BL1, which reaches its peak already after two seed entities.  This indicates the seed entities are better utilized in our embedding-based method.

%Out of the three baselines, BL1 performs far better than the other two in terms of both MAP and MRR. This indicates relations given by RDF triples are more beneficial for capturing entity similarity information. 
%For the combined methods, we only report the best performance at bottom block of Table~\ref{tbl:rows:rank}, \ld{and $\alpha$ is 0.4/0/0 for BL1/BL2/BL3 respectively which means the Table2VecE* is complementary with BL1 but not the other two baselines, and it also indicates Table2VecE* benefits from the candidate selection method by three literature baselines}. As observation, the combinations significantly outperform all four baseline methods ($p < 0.01$) and Table2VecE*. This means Table2VecE* and the candidates selection method of baselines complement each other. Furthermore, (BL1 + Table2VecE*) gives us the best performance in terms of MAP, which is consistent with above that BL1 gives the most relatedness information between entities. We also notice that the performance of combination methods improves with more seed entities, which indicates the seed entities are well utilized.
% without a decrease observed in baselines when the input numbers exceed $3$.


%\vspace*{-0.75\baselineskip}
\subsection{Column population}
\label{sec:eva:col}

Table~\ref{tbl:column:rank} shows column population performance.  We find that the combined method involving Table2VecH significantly outperforms the baseline method ($p < 0.01$) in terms of MAP when seed number exceeds 1, and when $|L|=3$ it achieves substantial and significant improvements ($p < 0.01$) both in terms of MAP and MRR.  Moreover, while the baseline performance does not improve with more seed column labels, the combined method can effectively utilize larger input sizes and keeps improving the performance. Combining these findings with the results obtained in Sect.~\ref{sec:eva:row}, we answer RQ1 positively.

According to Fig.~\ref{fig:col:alpha}, the interpolation parameter (cf. Eq.~\eqref{eq:cp:comb}) that yielded the best performance for the combined method is $\alpha=0.01$, which indicates Table2VecH similarity is assigned much more importance than the baseline.

%For both methods, performance improves along with more seed column labels, because more information is given for determining the related labels. 
%This phenomenon is consistent with that in our row population task.

%\vspace*{-0.5\baselineskip}
\subsection{Table retrieval}
\label{sec:eva:ret}
To answer RQ2 and RQ3, we list the table retrieval results in Table~\ref{tbl:results}. For Graph2Vec and Table2VecE, we achieve improvements over the baseline but these are not statistically significant. Table2VecW and Word2Vec have very comparable performance to each other and they outperform all other methods and significantly improve over baseline method ($p < 0.01$). The lack of difference between the two indicates that it does not make a difference for the table retrieval task whether word embeddings are trained specifically on tables or not (RQ2).
As for the different semantic representations (RQ3), these results show that word embeddings are more beneficial for table retrieval than entity embeddings. 
\input{other-inputs/tab-ret.tex}

In Fig.~\ref{fig:sub:qry}, we present the results for both our table embedding methods and baseline against two query subsets, QuerySet 1 and QuerySet 2, in terms of NDCG@20. We note that both our methods outperform the baseline on both query subsets. Table2VecW has the best performance among those three, which is consistent with our discover before in Table~\ref{tbl:results}. Moreover, out of three methods, the performance of QuerySet 1 exceeds that of QuerySet 2, which contains more specific queries.
\input{other-inputs/fig-sub-qrl.tex}